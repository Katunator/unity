﻿using MidiPlayerTK;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

namespace RGB_piano
{
    public class MusicController : MonoBehaviour
    {
        public const int noteCount = 88;

        public const int noteValueFirst = 21;
        public const int noteValueLast = 108;

        // sound player does more work (not only calculate events but also play them) and works slower as a result
        private const float speedMuliplierForPlayerNotes = 0.9875f;

        [SerializeField] private MidiFilePlayer playerNotes;
        [SerializeField] private MidiFilePlayer playerSound;

        [SerializeField] private float speed;

        [SerializeField] private MusicView musicView;

        private List<MidiFilePlayer> players;
        private Coroutine playCoroutine;

        public bool IsPaused
        {
            get { return playerNotes.MPTK_IsPaused; }
        }

        public string MidiName
        {
            get { return playerNotes.MPTK_MidiName; }
        }

        private void OnValidate()
        {
            //players.ForEach(p => p.MPTK_Speed = speed);
            playerSound.MPTK_Speed = speed;
            playerNotes.MPTK_Speed = speed * speedMuliplierForPlayerNotes;
        }

        private void Awake()
        {
            players = new List<MidiFilePlayer>()
            {
                playerNotes,
                playerSound
            };

            SetSpeed(speed);
        }

        private void Start()
        {
            StartPlayCoroutine();
        }

        private void SetSpeed(float value)
        {
            players.ForEach(p => p.MPTK_Speed = speed);
        }

        public void SetMidi(string name)
        {
            Stop();
            players.ForEach(p => p.MPTK_MidiName = name);
        }

        public void Play()
        {
            //Stop();

            StartPlayCoroutine();
        }

        private void StartPlayCoroutine()
        {
            if (playCoroutine != null)
            {
                StopCoroutine(playCoroutine);
            }

            playCoroutine = StartCoroutine(PlayMidiCoroutine());
        }

        private IEnumerator PlayMidiCoroutine()
        {
            playerNotes.MPTK_Play();

            yield return new WaitForSeconds(musicView.NoteFallTime);

            playerSound.MPTK_Play();
        }

        public void Stop()
        {
            if (IsPaused)
            {
                Continue();
            }

            players.ForEach(p => p.MPTK_Stop());

            musicView.Stop();
        }

        public void Pause()
        {
            players.ForEach(p => p.MPTK_Pause());

            musicView.Pause();
        }

        public void Continue()
        {
            players.ForEach(p => p.MPTK_UnPause());

            musicView.Continue();
        }
    }
}
