﻿using System;

public static class ActionExtension
{
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
        {
            action.Invoke();
        }
    }

    public static void SafeInvoke<T>(this Action<T> action, T p1)
    {
        if (action != null)
        {
            action.Invoke(p1);
        }
    }

    public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 p1, T2 p2)
    {
        if (action != null)
        {
            action.Invoke(p1, p2);
        }
    }
}
