﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class MidiListElementUI : MonoBehaviour
    {
        [SerializeField] private Text midiName;

        public event Action<MidiListElementUI> OnPressed;

        public string MidiName
        {
            get { return midiName.text; }
        }

        public void Init(string midiName)
        {
            this.midiName.text = midiName;
        }

        public void Press()
        {
            OnPressed.SafeInvoke(this);
        }
    }
}
