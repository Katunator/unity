﻿using MidiPlayerTK;
using System;
using UnityEngine;

namespace RGB_piano
{
    public class MidiListUI : CanvasUI
    {
        [SerializeField] private RectTransform content;
        [SerializeField] private MidiListElementUI elementPrefab;

        public event Action<string> OnMidiPressed;

        private void Start()
        {
            Hide();
            InstantiateMidies();
        }

        public void InstantiateMidies()
        {
            if (MidiPlayerGlobal.CurrentMidiSet != null
                && MidiPlayerGlobal.CurrentMidiSet.MidiFiles != null
                && MidiPlayerGlobal.CurrentMidiSet.MidiFiles.Count > 0)
            {
                foreach(var midi in MidiPlayerGlobal.CurrentMidiSet.MidiFiles)
                {
                    InstantiateMidi(midi);
                }
            }
        }

        private void InstantiateMidi(string name)
        {
            var midi = Instantiate(elementPrefab, content);
            midi.Init(name);
            midi.OnPressed += OnMidiPressedHandler;
        }

        private void OnMidiPressedHandler(MidiListElementUI midi)
        {
            Hide();
            OnMidiPressed(midi.MidiName);
        }
    }
}
