﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class ColorValueUI : ShaderValueUI
    {
        public static event Action<ColorValueUI> OnPressed;

        [SerializeField]
        private Image image;
        [SerializeField]
        private Image chosen;

        private FlexibleColorPicker colorPicker;
        private ColorValue value;

        public ColorValue Value
        {
            get { return value; }
        }

        private void Awake()
        {
            SetColors();
        }

        public override void Init(ShaderValue value)
        {
            this.value = value as ColorValue;
            SetColors();
        }

        private void SetColors()
        {
            if (image != null && value != null)
            {
                image.color = value.Color.color;
            }
        }

        public void SetChosen(bool chosen)
        {
            this.chosen.enabled = chosen;
        }

        public void Press()
        {
            OnPressed.SafeInvoke(this);
        }

        public void OnValueChangedHandler(Color color)
        {
            if (value != null)
            {
                value.Color.color = color;
                value.ApplyValue();
            }

            SetColors();
        }
    }
}
