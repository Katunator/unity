﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class NoteMaterialUI : CanvasUI
    {
        [SerializeField]
        private NoteMaterialController noteMaterialController;

        [SerializeField]
        private FlexibleColorPicker colorPicker;
        [SerializeField]
        private RectTransform parentTransform;
        [SerializeField]
        private MaterialValueContainerUI uiPrefab;

        private NoteMaterialDeciderUI defaultDecider;
        private NoteMaterialDeciderUI armsDecider;
        private NoteMaterialDeciderUI valueDecider;

        private NoteMaterialDeciderUI[] deciders;

        private NoteMaterialDeciderUI currentDecider;
        private ColorValueUI currentColorUI;

        private void Start()
        {
            InstantiateDecidersUi();

            ColorValueUI.OnPressed += OnUiPressedHandler;
        }

        public void RefreshUis()
        {
            DestroyDecidersUi();
            InstantiateDecidersUi();
            SwitchDeciderUI(noteMaterialController.colorMode);
        }

        private void InstantiateDecidersUi()
        {
            defaultDecider = InstantiateDeciderUi(noteMaterialController.defaultDecider);
            armsDecider = InstantiateDeciderUi(noteMaterialController.armsDecider);
            valueDecider = InstantiateDeciderUi(noteMaterialController.valueDecider);

            deciders = new NoteMaterialDeciderUI[]
            {
                defaultDecider,
                armsDecider,
                valueDecider
            };
        }

        private void DestroyDecidersUi()
        {
            foreach(var d in deciders)
            {
                for (int i = 0; i < d.uis.Count; i++)
                {
                    Destroy(d.uis[i].gameObject);
                }
            }
        }

        private NoteMaterialDeciderUI InstantiateDeciderUi(NoteMaterialDecider decider)
        {
            var uis = new List<MaterialValueContainerUI>();

            MaterialValueContainerUI ui;
            foreach (var m in decider.containers)
            {
                ui = Instantiate(uiPrefab, parentTransform);
                ui.Init(m);
                ui.gameObject.SetActive(false);
                uis.Add(ui);
            }

            return new NoteMaterialDeciderUI(uis);
        }

        public override void Show()
        {
            base.Show();

            RefreshUis();
        }

        public void PressDefault()
        {
            noteMaterialController.SetColorMode(NoteColorMode.Default);
            SwitchDeciderUI(NoteColorMode.Default);
        }

        public void PressArms()
        {
            noteMaterialController.SetColorMode(NoteColorMode.Arms);
            SwitchDeciderUI(NoteColorMode.Arms);
        }

        public void PressValue()
        {
            noteMaterialController.SetColorMode(NoteColorMode.Value);
            SwitchDeciderUI(NoteColorMode.Value);
        }

        private void SwitchDeciderUI(NoteColorMode mode)
        {
            foreach (var d in deciders)
            {
                d.SetActive(false);
            }

            currentDecider = GetDecider(mode);
            currentDecider.SetActive(true);
            OnUiPressedHandler(currentDecider.uis[0].BaseColor);
        }

        private NoteMaterialDeciderUI GetDecider(NoteColorMode mode)
        {
            switch (mode)
            {
                case NoteColorMode.Default:
                    return defaultDecider;
                case NoteColorMode.Arms:
                    return armsDecider;
                case NoteColorMode.Value:
                    return valueDecider;
            }

            return defaultDecider;
        }

        private void OnUiPressedHandler(ColorValueUI ui)
        {
            colorPicker.color = ui.Value.Color.color;
        }
    }
}
