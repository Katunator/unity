﻿using UnityEngine;

namespace RGB_piano
{
    public class CanvasUI : MonoBehaviour
    {
        [SerializeField] private Canvas canvas;

        private bool isShown
        {
            get { return canvas.enabled; }
        }

        public virtual void Show()
        {
            SetEnabled(true);
        }

        public virtual void Hide()
        {
            SetEnabled(false);
        }

        public void ShowHide()
        {
            if (isShown)
            {
                Hide();
            }
            else
            {
                Show();
            }
        }

        private void SetEnabled(bool enabled)
        {
            canvas.enabled = enabled;
        }
    }
}
