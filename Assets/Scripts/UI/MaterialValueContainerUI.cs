﻿using UnityEngine;

namespace RGB_piano
{
    public class MaterialValueContainerUI : MonoBehaviour
    {
        [SerializeField]
        private ColorValueUI baseColor;

        private MaterialValueContainer container;

        public ColorValueUI BaseColor
        {
            get { return baseColor; }
        }

        public void Init(MaterialValueContainer container)
        {
            this.container = container;

            baseColor.Init(container.ShaderConfigValue.BaseColorValue);
        }
    }
}
