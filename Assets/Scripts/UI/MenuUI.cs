﻿using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class MenuUI : CanvasUI
    {
        [SerializeField]
        private MusicController musicController;

        [SerializeField]
        private Text midiName;

        [SerializeField]
        private MidiListUI midiList;
        [SerializeField]
        private NoteMaterialUI colorList;
        [SerializeField]
        private PatternListUI patternList;

        private CanvasUI currentCanvasUI;

        private void Awake()
        {
            midiName.text = musicController.MidiName;
        }

        private void OnEnable()
        {
            midiList.OnMidiPressed += OnMidiChoosenHandler;
        }

        private void OnDisable()
        {
            midiList.OnMidiPressed -= OnMidiChoosenHandler;
        }

        public override void Show()
        {
            base.Show();

            //musicController.Pause();

            PressColor();
        }

        public override void Hide()
        {
            base.Hide();

            //musicController.Continue();

            HideCurrent();
        }

        public void PressPlayContinue()
        {
            if (musicController.IsPaused)
            {
                musicController.Continue();
            }
            else
            {
                musicController.Pause();
            }
        }

        public void PressSong()
        {
            SetCurCanvasUI(midiList);
        }

        public void PressColor()
        {
            SetCurCanvasUI(colorList);
        }

        public void PressPattern()
        {
            SetCurCanvasUI(patternList);
        }

        private void SetCurCanvasUI(CanvasUI canvasUI)
        {
            HideCurrent();
            currentCanvasUI = canvasUI;
            currentCanvasUI.Show();
        }

        private void HideCurrent()
        {
            currentCanvasUI?.Hide();
        }

        private void OnMidiChoosenHandler(string midiName)
        {
            this.midiName.text = midiName;
            musicController.SetMidi(midiName);
            musicController.Play();
            Hide();
        }
    }
}
