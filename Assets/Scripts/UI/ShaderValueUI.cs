﻿using UnityEngine;

namespace RGB_piano
{
    public abstract class ShaderValueUI : MonoBehaviour
    {
        public abstract void Init(ShaderValue value);
    }
}
