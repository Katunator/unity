﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class PatternElementUI : MonoBehaviour
    {
        public event Action<PatternElementUI> OnPressed;

        [SerializeField]
        private Image image;

        private MaterialParamContainer materialContainer;

        public MaterialParamContainer MaterialParamContainer
        {
            get { return materialContainer; }
        }

        public void Init(MaterialParamContainer material)
        {
            materialContainer = material;
            image.material = materialContainer.Material;
        }

        public void Press()
        {
            OnPressed.SafeInvoke(this);
        }
    }
}
