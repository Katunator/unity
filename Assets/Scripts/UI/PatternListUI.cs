﻿using UnityEngine;

namespace RGB_piano
{
    public class PatternListUI : CanvasUI
    {
        [SerializeField]
        private MaterialParamContainer[] patterns;

        [SerializeField]
        private NoteMaterialController materialController;

        [SerializeField]
        private RectTransform content;
        [SerializeField]
        private PatternElementUI elementPrefab;

        private void Start()
        {
            InstantiateElements();
        }

        public void InstantiateElements()
        {
            foreach (var p in patterns)
            {
                InstantiateElement(p);
            }
        }

        private void InstantiateElement(MaterialParamContainer material)
        {
            var pattern = Instantiate(elementPrefab, content);
            pattern.Init(material);
            pattern.OnPressed += OnMidiPressedHandler;
        }

        private void OnMidiPressedHandler(PatternElementUI pattern)
        {
            Debug.Log("UI set");
            materialController.SetMaterialContainer(pattern.MaterialParamContainer);
        }
    }
}
