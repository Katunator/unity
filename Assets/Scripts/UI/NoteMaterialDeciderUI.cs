﻿using System.Collections.Generic;
using UnityEngine;
using static FlexibleColorPicker;

namespace RGB_piano
{
    public class NoteMaterialDeciderUI
    {
        public List<MaterialValueContainerUI> uis;

        private static ColorValueUI currentColorUI;

        public NoteMaterialDeciderUI(List<MaterialValueContainerUI> uis)
        {
            this.uis = uis;

            BufferedColor.OnValueSet += OnColorChangedHandler;
            ColorValueUI.OnPressed += OnUiPressedHandler;
        }

        public void SetActive(bool active)
        {
            uis.ForEach(u => u.gameObject.SetActive(active));

            if (active)
            {
                OnUiPressedHandler(uis[0].BaseColor);
            }
        }

        private void OnUiPressedHandler(ColorValueUI ui)
        {
            currentColorUI?.SetChosen(false);

            currentColorUI = ui;
            currentColorUI.SetChosen(true);
        }

        private void OnColorChangedHandler(Color color)
        {
            currentColorUI?.OnValueChangedHandler(color);
        }
    }
}
