﻿using UnityEngine;

namespace RGB_piano
{
    public class MusicView : MonoBehaviour
    {
        [SerializeField] private SheetView sheetView;
        [SerializeField] private PianoView pianoView;

        public float NoteFallTime
        {
            get { return sheetView.NoteFallTime; }
        }

        private void Awake()
        {

        }

        public void Pause()
        {
            sheetView.Pause();
        }

        public void Continue()
        {
            sheetView.Continue();
        }

        public void Play()
        {

        }

        public void Stop()
        {
            sheetView.Clear();
            pianoView.Clear();
        }
    }
}
