﻿using UnityEngine;

namespace RGB_piano
{
    [CreateAssetMenu(menuName = "SO/MaterialParamContainer")]
    public class MaterialParamContainer : ScriptableObject
    {
        [SerializeField] private Material material;
        [SerializeField] private ShaderConfigParam shaderConfig;

        public Material Material
        {
            get { return material; }
        }

        public ShaderConfigParam ShaderConfig
        {
            get { return shaderConfig; }
        }

        public MaterialValueContainer GetMaterialValueContainer(ColorLink baseColor = null)
        {
            Material newMaterial = Instantiate(material);

            return new MaterialValueContainer(
                newMaterial,
                shaderConfig.GetShaderConfigValue(newMaterial, baseColor));
        }
    }
}
