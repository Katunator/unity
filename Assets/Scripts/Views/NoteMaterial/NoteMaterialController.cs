﻿using UnityEngine;

namespace RGB_piano
{
    public class NoteMaterialController : MonoBehaviour
    {
        public NoteColorMode colorMode;

        public MaterialParamContainer materialContainer;

        public DefaultDecider defaultDecider;
        public ArmsDecider armsDecider;
        public ValueDecider valueDecider;

        public SheetView sheetView;

        private NoteMaterialDecider noteDecider;

        public NoteMaterialDecider NoteMaterialDecider
        {
            get { return noteDecider; }
        }

        private void Awake()
        {
            SetMaterialContainer();

            SetColorMode(NoteColorMode.Default);
        }

        public void SetMaterialContainer(MaterialParamContainer materialParamContainer)
        {
            Debug.Log("contr set");
            materialContainer = materialParamContainer;
            SetMaterialContainer();
        }

        private void SetMaterialContainer()
        {
            defaultDecider.Init(materialContainer);
            armsDecider.Init(materialContainer);
            valueDecider.Init(materialContainer);

            RefreshAll();
        }

        public void SetColorMode(NoteColorMode mode)
        {
            colorMode = mode;

            noteDecider = GetDecider(mode);

            noteDecider.ApplyValues();
            RefreshAll();
        }

        private NoteMaterialDecider GetDecider(NoteColorMode mode)
        {
            switch (mode)
            {
                case NoteColorMode.Default:
                    return defaultDecider;
                case NoteColorMode.Arms:
                    return armsDecider;
                case NoteColorMode.Value:
                    return valueDecider;
            }

            return defaultDecider;
        }

        private void RefreshAll()
        {
            foreach (var n in sheetView.Notes)
            {
                SetMaterial(n);
            }
        }

        public void SetMaterial(NoteView note)
        {
            note.SetMaterial(noteDecider.GetMaterial(note.Note));
        }
    }

    public enum NoteColorMode
    {
        Default,
        Arms,
        Value
    }
}
