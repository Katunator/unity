﻿using UnityEngine;

namespace RGB_piano
{
    public class MaterialValueContainer
    {
        private Material material;
        private ShaderConfigValue shaderConfigValue;

        public Material Material
        {
            get { return material; }
        }

        public ShaderConfigValue ShaderConfigValue
        {
            get { return shaderConfigValue; }
        }

        public MaterialValueContainer(Material material, ShaderConfigValue shaderConfigValue)
        {
            this.material = material;
            this.shaderConfigValue = shaderConfigValue;
        }

        public void SetValues()
        {
            shaderConfigValue.SetValues();
        }
    }
}
