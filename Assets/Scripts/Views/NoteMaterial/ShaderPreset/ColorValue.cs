﻿using System;
using UnityEngine;

namespace RGB_piano
{
    public class ColorValue : ShaderValue
    {
        private string param;
        private ColorLink color;

        public string Param
        {
            get { return param; }
        }

        public ColorLink Color
        {
            get { return color; }
            set { color = value; }
        }

        public ColorValue(string param, Material material, ColorLink color = null)
        {
            this.param = param;
            this.color = color ?? new ColorLink(material.GetColor(param));
            this.material = material;
        }

        public override void ApplyValue()
        {
            material.SetColor(param, color.color);
        }
    }

    [Serializable]
    public class ColorLink
    {
        public Color color;

        public ColorLink(Color color)
        {
            this.color = color;
        }
    }
}
