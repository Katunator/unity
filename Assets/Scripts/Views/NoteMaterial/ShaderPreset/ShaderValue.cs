﻿using UnityEngine;

namespace RGB_piano
{
    public abstract class ShaderValue
    {
        protected Material material;

        public abstract void ApplyValue();
    }
}
