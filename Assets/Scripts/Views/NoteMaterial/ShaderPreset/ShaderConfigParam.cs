﻿using System.Collections.Generic;
using UnityEngine;

namespace RGB_piano
{
    [CreateAssetMenu(menuName = "SO/Shader/Preset")]
    public class ShaderConfigParam : ScriptableObject
    {
        [SerializeField]
        private ColorParam baseColorParam;

        [SerializeField]
        private ShaderParam[] otherParams;

        public ShaderConfigValue GetShaderConfigValue(Material material, ColorLink color = null)
        {
            var shaderValues = new List<ShaderValue>();

            for (int i = 0; i < otherParams.Length; i++)
            {
                shaderValues.Add(otherParams[i].GetShaderValue(material));
            }

            return new ShaderConfigValue(
                baseColorParam.GetShaderValue(material, color),
                shaderValues);
        }
    }
}
