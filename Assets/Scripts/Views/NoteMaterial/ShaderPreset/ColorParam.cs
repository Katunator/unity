﻿using UnityEngine;

namespace RGB_piano
{
    [CreateAssetMenu(menuName = "SO/Shader/Param/Color")]
    public class ColorParam : ShaderParam
    {
        public ColorValue GetShaderValue(Material material, ColorLink baseColor = null)
        {
            return new ColorValue(paramName, material, baseColor);
        }

        public override ShaderValue GetShaderValue(Material material)
        {
            return new ColorValue(paramName, material);
        }
    }
}
