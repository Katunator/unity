﻿using System.Collections.Generic;
using UnityEngine;

namespace RGB_piano
{
    public class ShaderConfigValue
    {
        private ColorValue baseColorValue;
        private List<ShaderValue> otherValues;

        public ColorValue BaseColorValue
        {
            get { return baseColorValue; }
        }

        public List<ShaderValue> OtherValues
        {
            get { return otherValues; }
        }

        public ShaderConfigValue(ColorValue baseColorValue, List<ShaderValue> otherValues)
        {
            this.baseColorValue = baseColorValue;
            this.otherValues = otherValues;
        }

        public void SetValues()
        {
            baseColorValue.ApplyValue();
            otherValues.ForEach(v => v.ApplyValue());
        }
    }
}
