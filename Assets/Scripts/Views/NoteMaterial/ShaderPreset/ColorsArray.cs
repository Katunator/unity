﻿using UnityEngine;

namespace RGB_piano
{
    [CreateAssetMenu(menuName ="SO/ColorsArray")]
    public class ColorsArray : ScriptableObject
    {
        public Color[] colors;
    }
}
