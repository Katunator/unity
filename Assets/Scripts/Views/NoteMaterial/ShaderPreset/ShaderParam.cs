﻿using UnityEngine;

namespace RGB_piano
{
    public abstract class ShaderParam : ScriptableObject
    {
        [SerializeField]
        protected string paramName;

        public abstract ShaderValue GetShaderValue(Material material);
    }
}
