﻿using MidiPlayerTK;

namespace RGB_piano
{
    public class DefaultDecider : ArrayBaseDecider
    {
        protected override int GetMaterialIndex(MPTKEvent note)
        {
            return 0;
        }
    }
}
