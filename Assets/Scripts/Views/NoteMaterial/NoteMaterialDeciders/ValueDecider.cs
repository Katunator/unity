﻿using MidiPlayerTK;

namespace RGB_piano
{
    public class ValueDecider : ArrayBaseDecider
    {
        protected override int GetMaterialIndex(MPTKEvent note)
        {
            return PianoView.GetNoteOctaveIndex(note.Value);
        }
    }
}
