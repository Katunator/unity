﻿using MidiPlayerTK;
using UnityEngine;

namespace RGB_piano
{
    public abstract class ArrayBaseDecider : NoteMaterialDecider
    {
        [SerializeField]
        private ColorsArray initColors;

        private ColorLink[] colors;

        private void Awake()
        {
            colors = new ColorLink[initColors.colors.Length];
            for (int i = 0; i < initColors.colors.Length; i++)
            {
                colors[i] = new ColorLink(initColors.colors[i]);
            }
        }

        protected override void CreateMaterialContainers(MaterialParamContainer container)
        {
            base.CreateMaterialContainers(container);

            for (int i = 0; i < colors.Length; i++)
            {
                containers.Add(container.GetMaterialValueContainer(colors[i]));
            }

            ApplyValues();
        }

        public override Material GetMaterial(MPTKEvent note)
        {
            int index = GetMaterialIndex(note);
            if (index > 0
                && index < containers.Count)
            {
                return containers[index].Material;
            }

            return containers[0].Material;
        }

        protected abstract int GetMaterialIndex(MPTKEvent note);
    }
}
