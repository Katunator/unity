﻿using MidiPlayerTK;

namespace RGB_piano
{
    public class ArmsDecider : ArrayBaseDecider
    {
        protected override int GetMaterialIndex(MPTKEvent note)
        {
            return (int)note.Track - 1;
        }
    }
}
