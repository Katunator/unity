﻿using MidiPlayerTK;
using System.Collections.Generic;
using UnityEngine;

namespace RGB_piano
{
    public abstract class NoteMaterialDecider : MonoBehaviour
    {
        public List<MaterialValueContainer> containers;

        public void Init(MaterialParamContainer container)
        {
            CreateMaterialContainers(container);
        }

        protected virtual void CreateMaterialContainers(MaterialParamContainer material)
        {
            containers = new List<MaterialValueContainer>();
        }

        public abstract Material GetMaterial(MPTKEvent note);

        public void ApplyValues()
        {
            containers.ForEach(m => m.SetValues());
        }
    }
}
