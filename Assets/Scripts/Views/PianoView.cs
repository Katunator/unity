﻿using MidiPlayerTK;
using System.Collections.Generic;
using UnityEngine;

namespace RGB_piano
{
    public class PianoView : MonoBehaviour
    {
        private const int whiteKeyCount = 52;
        public const int octaveNoteCount = 12;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private RectTransform whiteKeys;
        [SerializeField]
        private RectTransform blackKeys;
        [SerializeField]
        private RectTransform musicRect;

        [SerializeField]
        private KeyView keyPrefab;
        [SerializeField]
        private RectTransform octaveStrip;

        [SerializeField]
        private Color[] colors;

        private float keyPositionStepX;
        private float keyWidth;
        private int keyCount
        {
            get { return MusicController.noteCount; }
        }

        // int: note value
        private Dictionary<int, KeyView> keys;

        private void Start()
        {
            keyWidth = rect.rect.width / whiteKeyCount;
            keys = new Dictionary<int, KeyView>();

            float keyX = 0f;
            bool isBlack;
            int keyValue = MusicController.noteValueFirst;

            for (int i = 0; i < keyCount; i++)
            {
                isBlack = IsBlackKey(i);

                keys.Add(keyValue,
                    InstantiateKey(
                        i,
                        isBlack,
                        isBlack ? keyX - keyWidth / 4f : keyX));

                if (GetKeyOctaveIndex(i) == 0)
                {
                    RectTransform strip = Instantiate(octaveStrip, musicRect);
                    strip.localPosition = new Vector3(keyX, 0f, 0f);
                    strip.gameObject.SetActive(true);
                }

                if (!isBlack)
                {
                    keyX += keyWidth;
                }

                keyValue++;
            }
        }

        private void OnEnable()
        {
            NoteView.OnPlayStart += OnNotePlayStartHandler;
            NoteView.OnPlayFinish += OnNotePlayFinishHandler;
        }

        private void OnDisable()
        {
            NoteView.OnPlayStart -= OnNotePlayStartHandler;
            NoteView.OnPlayFinish -= OnNotePlayFinishHandler;
        }

        private KeyView InstantiateKey(int index, bool isBlack, float positionX)
        {
            KeyView key = Instantiate(keyPrefab, isBlack ? blackKeys : whiteKeys);
            key.transform.localPosition = new Vector3(positionX, 0, 0);

            float width = isBlack ? keyWidth / 2f : keyWidth;

            key.Init(width, index, GetKeyOctaveIndex(index), IsBlackKey(index), GetKeyColor(GetKeyOctaveIndex(index)));

            return key;
        }

        private float GetKeyPositionX(int value)
        {
            return keys[value].LocalPositionX;
        }

        private bool IsBlackKey(int index)
        {
            switch (GetKeyOctaveIndex(index))
            {
                case 0: return false;
                case 1: return true;
                case 2: return false;
                case 3: return true;
                case 4: return false;
                case 5: return false;
                case 6: return true;
                case 7: return false;
                case 8: return true;
                case 9: return false;
                case 10: return true;
                case 11: return false;
            }

            return false;
        }

        public static int GetNoteOctaveIndex(int noteValue)
        {
            return noteValue % octaveNoteCount;
        }

        private int GetKeyOctaveIndex(int index)
        {
            return (index + 9) % octaveNoteCount;
        }

        private Color GetKeyColor(int octaveIndex)
        {
            return colors[octaveIndex];
        }

        private KeyView GetKey(NoteView note)
        {
            return GetKey(note.Note);
        }

        public KeyView GetKey(MPTKEvent note)
        {
            return keys[note.Value];
        }

        public void Clear()
        {
            for (int i = MusicController.noteValueFirst; i < MusicController.noteValueLast + 1; i++)
            {
                keys[i].KeyUp();
            }
        }

        private void OnNotePlayStartHandler(NoteView note)
        {
            GetKey(note).KeyDown(note.Material);
            Debug.Log(note + " " + note.Note + " " + GetKey(note) + " " + note.Material);
        }

        private void OnNotePlayFinishHandler(NoteView note)
        {
            GetKey(note).KeyUp();
        }
    }
}