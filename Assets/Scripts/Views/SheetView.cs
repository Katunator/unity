﻿using System.Collections.Generic;
using UnityEngine;
using MidiPlayerTK;

namespace RGB_piano
{
    public class SheetView : MonoBehaviour
    {
        public static float Speed;

        [SerializeField]
        private MidiFilePlayer midiFilePlayer;
        [SerializeField]
        private PianoView pianoView;
        [SerializeField]
        private NoteMaterialController materialController;

        [SerializeField]
        private NoteView notePrefab;
        [SerializeField]
        private float speed = 150f;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private RectTransform whiteNotes;
        [SerializeField]
        private RectTransform blackNotes;

        private Dictionary<int, NoteView> lastNotes; // int: note value
        private List<NoteView> notes;
        private Stack<NoteView> notesPool;

        public List<NoteView> Notes
        {
            get { return notes; }
        }

        private int noteCount
        {
            get { return MusicController.noteCount; }
        }

        public float NoteFallTime
        {
            get { return rect.rect.height / Speed; }
        }

        private void Awake()
        {
            InitNoteDictionary();

            notes = new List<NoteView>();
            notesPool = new Stack<NoteView>();

            Speed = speed;
        }

        private void InitNoteDictionary()
        {
            lastNotes = new Dictionary<int, NoteView>();
            for (int i = MusicController.noteValueFirst; i < MusicController.noteValueLast + 1; i++)
            {
                lastNotes.Add(i, null);
            }
        }

        private void OnEnable()
        {
            NoteView.OnPlayFinish += OnNotePlayFinishHandler;

            SubscribeOnPlayer();
        }

        private void OnDisable()
        {
            NoteView.OnPlayFinish -= OnNotePlayFinishHandler;
        }

        private void SubscribeOnPlayer()
        {
            if (midiFilePlayer != null)
            {
                if (!midiFilePlayer.OnEventNotesMidi.HasEvent())
                {
                    midiFilePlayer.OnEventNotesMidi.AddListener(OnEventNoteMidiHandler);
                }
            }
        }

        private void OnEventNoteMidiHandler(List<MPTKEvent> notes)
        {
            foreach (MPTKEvent note in notes)
            {
                if (note.Command == MPTKCommand.NoteOn
                    && note.Value >= MusicController.noteValueFirst
                    && note.Value <= MusicController.noteValueLast)
                {
                    InstantiateNote(note);
                }
            }
        }

        private void InstantiateNote(MPTKEvent note)
        {
            int value = note.Value;
            int index = value - MusicController.noteValueFirst;
            KeyView key = pianoView.GetKey(note);

            NoteView n = GetNoteInstance();

            n.transform.SetParent(key.IsBlack ? blackNotes : whiteNotes);
            n.transform.localPosition = GetNotePosition(key);

            float height = GetNoteHeight(note.Duration);
            float width = GetNoteWidth(key);

            n.Init(note, width, height, key.IsBlack);

            materialController.SetMaterial(n);

            CheckNoteOverlapping(n, lastNotes[value]);

            lastNotes[value] = n;
            notes.Add(n);
        }

        private NoteView GetNoteInstance()
        {
            if (notesPool.Count > 0)
            {
                return notesPool.Pop();
            }

            return Instantiate(notePrefab);
        }

        private void CheckNoteOverlapping(NoteView note, NoteView prewNote)
        {
            if (prewNote != null)
            {
                float overlappingDelta = prewNote.LocalPositionHeight - note.LocalPositionY;
                if (overlappingDelta > 0f)
                {
                    prewNote.ChangeHeight(-(overlappingDelta + 0.1f));
                }
            }
        }

        private Vector3 GetNotePosition(KeyView key)
        {
            return new Vector3(
                key.LocalPositionX,
                rect.rect.height,
                0);
        }

        private float GetNoteHeight(float durationMilisec)
        {
            return Speed * durationMilisec * 0.001f;
        }

        private float GetNoteWidth(KeyView key)
        {
            return key.Width;
        }

        private void OnNotePlayFinishHandler(NoteView note)
        {
            if (lastNotes[note.Note.Value] == note)
            {
                lastNotes[note.Note.Value] = null;
            }

            notes.Remove(note);
            notesPool.Push(note);
        }

        public void Continue()
        {
            Speed = speed;
        }

        public void Pause()
        {
            Speed = 0f;
        }

        public void Clear()
        {
            var notesClone = new NoteView[notes.Count];
            notes.CopyTo(notesClone);
            foreach (var n in notesClone)
            {
                OnNotePlayFinishHandler(n);
                n.SetActive(false);
            }
        }
    }
}