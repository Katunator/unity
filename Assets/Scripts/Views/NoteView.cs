﻿using MidiPlayerTK;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class NoteView : MonoBehaviour
    {
        public static event Action<NoteView> OnPlayStart;
        public static event Action<NoteView> OnPlayFinish;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private Image image;

        private MPTKEvent note;
        private bool isBlack;

        private bool played = false;

        public float LocalPositionY
        {
            get { return rect.localPosition.y; }
        }

        public float LocalPositionHeight
        {
            get { return LocalPositionY + rect.rect.height; }
        }

        public MPTKEvent Note
        {
            get { return note; }
        }

        public Material Material
        {
            get { return image.material; }
        }

        public void Init(MPTKEvent note, float width, float height, bool isBlack)
        {
            this.note = note;
            this.isBlack = isBlack;

            played = false;

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            SetHeight(height);

            SetActive(true);
        }

        public void ChangeHeight(float amount)
        {
            SetHeight(rect.rect.height + amount);
        }

        private void SetHeight(float value)
        {
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value);
        }

        public void SetMaterial(Material material)
        {
            if (image != null)
            {
                image.material = material;
            }
        }

        private void FixedUpdate()
        {
            Translate();

            CheckStates();
        }

        private void Translate()
        {
            float translation = Time.fixedDeltaTime * SheetView.Speed;
            rect.localPosition += Vector3.down * translation;
        }

        private void CheckStates()
        {
            if (!played && transform.localPosition.y < 0f)
            {
                OnPlayStart.SafeInvoke(this);

                played = true;
            }
            if (transform.localPosition.y < -rect.rect.height)
            {
                OnPlayFinish.SafeInvoke(this);

                SetActive(false);
            }
        }

        public void SetActive(bool active)
        {
            gameObject.SetActive(active);
        }
    }
}