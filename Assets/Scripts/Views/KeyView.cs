﻿using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class KeyView : MonoBehaviour
    {
        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private Image image;

        [SerializeField]
        private ParticleSystem keyDownParticles;

        private int index;
        private int octaveIndex;
        private bool isBlack;

        private Material initMaterial;

        private Color initColor;
        private Color pressedColor;

        public float Width
        {
            get { return rect.rect.width; }
        }

        public float LocalPositionX
        {
            get { return rect.localPosition.x; }
        }

        public bool IsBlack
        {
            get { return isBlack; }
        }

        public void Init(float width, int index, int octaveIndex, bool isBlack, Color color)
        {
            this.index = index;
            this.octaveIndex = octaveIndex;
            this.isBlack = isBlack;

            initColor = isBlack ? Color.black : Color.white;
            pressedColor = Color.white;
            initMaterial = image.material;

            ResetColor();

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            if (isBlack)
            {
                rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0.6f * rect.rect.height);
                Vector3 pos = rect.position;
                pos.z = 10f;
                rect.position = pos;
            }

            gameObject.SetActive(true);
        }

        private void ResetColor()
        {
            SetColor(initColor);
            image.material = initMaterial;
        }

        private void SetColor(Color color)
        {
            color.a = 1;
            image.color = color;
        }

        public void KeyDown(Material material)
        {
            SetColor(pressedColor);
            image.material = material;
            keyDownParticles.Play();
        }

        public void KeyUp()
        {
            ResetColor();

            keyDownParticles.Stop();
        }
    }
}
